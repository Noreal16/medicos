'use strict';
var pacienteC = require('../modelos/paciente');
var historialC = require('../modelos/historial');
class pacienteControler {
    visualizar(req, res) {

        pacienteC.getJoin({ historial: true }).then(function(todos) {
            var nro = "NHIS-" + (todos.length + 1);
            res.render('index1', {
                title: 'Pacientes',
                fragmento: "fragmentos/paciente",
                sesion: true,
                listado: todos,
                nro: nro,
                msg: { error: req.flash('error'), info: req.flash('info') }
            });
        }).error(function(error) {
            req.flash('error', 'Hubo un error');
            res.redirect('/');
        });

    }
    guardarP(req, res) {
        var dataP = {
            cedula: req.body.cedula,
            apellidos: req.body.apellido,
            nombres: req.body.nombre,
            direccion: req.body.direccion
        }
        var paciente = new pacienteC(dataP);
        var historiaP = {
            contacto: req.body.contacto,
            enfermedades: req.body.enfermedades,
            enfermedades_hereditarias: req.body.enfermedades_h,
            habitos: req.body.habito,
            nro_historial: req.body.nro_historia
        }
        var historia = new historialC(historiaP);
        paciente.historia = historia;
        paciente.saveAll({ historia: true }).the(function(nuevo) {
            req.flash('info', 'Paciente registrado ');
            res.redirect('/paciente')
        }).error(function(error) {
            req.flash('error', 'No se pudo registrar ');
            res.redirect('/paciente')
        });
    }
    visualizar_modificar(req, res) {
        var external = req.params.external;
        pacienteC.getJoin({ historial: true }).filter({ external_id: external }).then(function(data) {
            if (data.length > 0) {
                var paciente1 = data[0];
                res.render('index1', {
                    title: 'Paciente',
                    fragmento: "fragmentos/paciente/modificar",
                    sesion: true,
                    paci: paciente1,
                    msg: { error: req.flash('error'), info: req.flash('info') }
                })
            } else {
                req.flash('error', 'No se pudo encontra lo solicitado!');
                res.redirect('/paciente')
            }
        }).error(function(error) {

        });
    };
}

module.exports = pacienteControler;
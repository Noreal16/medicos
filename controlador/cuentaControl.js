'use strict';
var cuentaC = require('../modelos/cuenta');

class cuentaControl {
    /**
     * Funcion que permite iniciar secion y cerrar sesion
     * @param {type} req Objeto Peticion
     * @param {type} res Objeto respuesta
     */
    iniciarSesion(req, res) {
        //var cuenta = new cuentaC();
        cuentaC.getJoin({ medico: true }).filter({ correo: req.body.correo }).run().then(function(resultado) {
            if (resultado.length > 0) {
                var cuenta = resultado[0];
                if (cuenta.clave === req.body.clave) {
                    req.session.cuenta = {
                        external: cuenta.medico.external_id,
                        usuatio: cuenta.medico.apellidos + " " + cuenta.medico.nombres
                    };
                    res.redirect('/');
                } else {
                    req.flash('error', 'Sus credenciales son incorrectas!');
                    res.redirect('/');
                }

            } else {
                req.flash('error', 'Sus credenciales son incorrectas!');
                res.redirect('/');
            }

        }).error(function(error) {

        });
    }
    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect('/');
    }
}
module.exports = cuentaControl;
var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var HistoriaClinica = thinky.createModel("HistoriaClinica", {
    id: type.string(),
    diagnostico: type.string(),
    fecha: type.date(),
    motivo: type.string(),
    receta: type.string(),
    createAt: type.date().default(r.now()),
    updateAt: type.date().default(r.now()),
    id_historial: type.string(),
    id_medico: type.string()
});
module.exports = HistoriaClinica;
var historial = require("./historial");
HistoriaClinica.belongsTo(historial, "historial", "id", "id_historial");
var Medico = require("./medico");
HistoriaClinica.belongsTo(Medico, "medico", "id", "id_medico");

var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Paciente = thinky.createModel("Paciente", {
    id: type.string(),
    apellidos: type.string(),
    cedula: type.string(),
    direcion: type.string(),
    esternal_id: type.string().default(r.uuid()),
    nombres: type.string(),
    createAt: type.date().default(r.now()),

});
module.exports = Paciente;
var Historial = require("./historial");
Paciente.hasOne(Historial, "historial", "id_paciente", "id");